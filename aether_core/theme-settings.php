<?php

require_once drupal_get_path('theme', 'aether') . '/includes/theme-base.inc';
require_once drupal_get_path('theme', 'aether') . '/includes/theme.inc';

/**
 * @file
 * Contains theme override functions for Theme Settings.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function aether_form_system_theme_settings_alter(&$form, $form_state) {
  // Required due to a D7 bug that causes the theme to load twice.
  // If this function is loaded a second time we return immediately
  // to prevent further complications.
  global $aether_altered;
  if ($aether_altered) return;
  $aether_altered = TRUE;
  $theme_name = $GLOBALS['theme_key'];
  $theme = aether_get_theme();
  $aether_theme_path = drupal_get_path('theme', 'aether') . '/';

  // Assign shorter names forto grid info.
  $regions = $theme->grid_regions;
  $grid_settings = $theme->grid_settings;
  $grid_layouts = $theme->grid_layouts;

  // Gather the grid adjusted regions.
  $grid_regions = $theme->grid_settings['regions'];

  // Get default theme settings from .info file.
  // get data for all themes.
  $theme_data = list_themes();
  $defaults = ($theme_name && isset($theme_data[$theme_name]->info['settings'])) ? $theme_data[$theme_name]->info['settings'] : array();

  drupal_add_library('system', 'ui.tabs');
  drupal_add_js($aether_theme_path . "js/jquery.autotabs.js", array('type' => 'file', 'scope' => 'header', 'weight' => 8));
  drupal_add_js('$(function () {Drupal.behaviors.formUpdated = null;});', 'inline');
  drupal_add_css('themes/seven/jquery.ui.theme.css', array('group' => CSS_THEME, 'weight' => 11));
  drupal_add_css('themes/seven/vertical-tabs.css', array('group' => CSS_THEME, 'weight' => 12));
  drupal_add_css($aether_theme_path . 'css/layout/layout-theme-settings.css', array('group' => CSS_THEME, 'weight' => 13));

  $header  = '<div class="themesettings-header">';
  $header .= '<h3>' . drupal_ucfirst($theme_name) . ' ' . t('Configuration') . '</h3>';
  $header .= '</div>';

  $form['aether_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 0,
    '#prefix' => $header,
  );

  require $aether_theme_path . 'includes/layout-theme-settings.inc';

  $form['aether_settings']['polyfills'] = array(
    '#title' => t('Polyfills'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );
  $form['aether_settings']['polyfills']['aether_polyfills'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Add HTML5 and responsive scripts and meta tags to every page.'),
    '#default_value' => theme_get_setting('aether_polyfills'),
    '#options'       => array(
      'respond' => t('Add respondJS JavaScript to add responsive support to IE 7-8.'),
      'fixedie7' => t('Add a fixed-width stylesheet to IE 7.'),
      'fixedie8' => t('Add a fixed-width stylesheet to IE 8 or below.'),
      'html5' => t('Add HTML5shim JavaScript to add HTML5 support to IE 7-8.'),
      'meta' => t('Add meta tags to support responsive design on mobile devices.'),
      'selectivizr' => t('Add pseudo class support to IE7-8.'),
      'imgsizer' => t('Add imgsizer fluid image support to IE7-8.'),
      'ios_orientation_fix' => t('Add fix for the iOS orientationchange zoom bug.'),
      'responsive_tables' => t('Adds js for responsive tables, use the class .responsive on a table to activate.'),
    ),
    '#description'   => t('IE 7-8 require a JavaScript polyfill solution to add basic support of HTML5. If you prefer to use another polyfill solution, such as <a href="!link">Modernizr</a>, you can disable these options. Mobile devices require a few meta tags for responsive designs.', array('!link' => 'http://www.modernizr.com/')),
  );
  $form['aether_settings']['css'] = array(
    '#title' => t('CSS'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 11,
  );
  $form['aether_settings']['css']['exclude_stylesheets'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Disable core CSS, check the stylesheets you wish to EXCLUDE.'),
    '#default_value' => theme_get_setting('exclude_stylesheets'),
    '#options'       => array(
      'misc/vertical-tabs.css' => t('misc/vertical-tabs.css'),
      'modules/aggregator/aggregator.css' => t('modules/aggregator/aggregator.css'),
      'modules/block/block.css' => t('modules/block/block.css'),
      'modules/dblog/dblog.css' => t('modules/dblog/dblog.css'),
      'modules/file/file.css' => t('modules/file/file.css'),
      'modules/filter/filter.css' => t('modules/filter/filter.css'),
      'modules/help/help.css' => t('modules/help/help.css'),
      'modules/menu/menu.css' => t('modules/menu/menu.css'),
      'modules/openid/openid.css' => t('modules/openid/openid.css'),
      'modules/profile/profile.css' => t('modules/profile/profile.css'),
      'modules/statistics/statistics.css' => t('modules/statistics/statistics.css'),
      'modules/syslog/syslog.css' => t('modules/syslog/syslog.css'),
      'modules/system/admin.css' => t('modules/system/admin.css'),
      'modules/system/maintenance.css' => t('modules/system/maintenance.css'),
      'modules/system/system.css' => t('modules/system/system.css'),
      'modules/system/system.admin.css' => t('modules/system/system.admin.css'),
      'modules/system/system.base.css' => t('modules/system/system.base.css'),
      'modules/system/system.maintenance.css' => t('modules/system/system.maintenance.css'),
      'modules/system/system.menus.css' => t('modules/system/system.menus.css'),
      'modules/system/system.messages.css' => t('modules/system/system.messages.css'),
      'modules/system/system.theme.css' => t('modules/system/system.theme.css'),
      'modules/taxonomy/taxonomy.css' => t('modules/taxonomy/taxonomy.css'),
      'modules/tracker/tracker.css' => t('modules/tracker/tracker.css'),
      'modules/update/update.css' => t('modules/update/update.css'),
    ),
    '#description'   => t('optional CSS'),
  );
  $form['aether_settings']['drupal'] = array(
    '#title' => t('Drupal core options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 12,
  );
  $form['aether_settings']['drupal']['aether_breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb settings'),
    '#attributes'    => array('id' => 'aether-breadcrumb'),
  );
  $form['aether_settings']['drupal']['aether_breadcrumb']['aether_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('aether_breadcrumb'),
    '#options'       => array(
      'yes'   => t('Yes'),
      'admin' => t('Only in admin section'),
      'no'    => t('No'),
    ),
  );
  $form['aether_settings']['drupal']['aether_breadcrumb']['aether_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => theme_get_setting('aether_breadcrumb_separator'),
    '#size'          => 5,
    '#maxlength'     => 10,
    '#prefix'        => '<div id="div-aether-breadcrumb-collapse">',
    // Jquery hook to show/hide optional widgets.
  );
  $form['aether_settings']['drupal']['aether_breadcrumb']['aether_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => theme_get_setting('aether_breadcrumb_home'),
  );
  $form['aether_settings']['drupal']['aether_breadcrumb']['aether_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('aether_breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['aether_settings']['drupal']['aether_breadcrumb']['aether_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('aether_breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
    '#suffix'        => '</div>',
  );
  $form['aether_settings']['themedev'] = array(
    '#title' => t('Debugging'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 12,
  );
  $form['aether_settings']['themedev']['aether_skip_link_anchor'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Anchor ID for the “skip link”'),
    '#default_value' => theme_get_setting('aether_skip_link_anchor'),
    '#field_prefix'  => '#',
    '#description'   => t('Specify the HTML ID of the element that the accessible-but-hidden “skip link” should link to. (<a href="!link">Read more about skip links</a>.)', array('!link' => 'http://drupal.org/node/467976')),
  );
  $form['aether_settings']['themedev']['grid_background'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Grid debugging'),
    '#default_value' => theme_get_setting('grid_background'),
    '#options'       => array(
      '1' => t("Enable grid background to aid the design process."),
    ),
    '#description'   => t('Enable or disable various grid and responsive layout options'),
  );
  $form['aether_settings']['themedev']['wireframe_mode'] = array(
  '#type' => 'checkbox',
  '#title' =>  t('Wireframe Mode: Display borders around layout elements'),
  '#description' => t('Wireframes are useful when prototyping a website'),
  '#default_value' => theme_get_setting('wireframe_mode'),
  );
  $form['aether_settings']['themedev']['clear_registry'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Rebuild theme registry on every page.'),
    '#description'   => t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#default_value' => theme_get_setting('clear_registry'),
  );
}
