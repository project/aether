<?php
/**
 * @file
 * Implements region preprocess functions.
 */

/**
 * Preprocess variables for region.tpl.php.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case).
 */
function aether_preprocess_region(&$variables, $hook) {

  global $theme_key;
  $theme = aether_get_theme();

  // Gather the grid region info.
  $grid_regions = $theme->grid_regions;

  // Initialize grid info once per page.
  static $grid = NULL;
  if (!isset($grid)) {
    $grid = aether_grid_info();
  }

  // Pull in main_menu vars from page preprocessor.
  $variables['main_menu'] = _aether_var('main_menu');

  foreach ($grid_regions as $grid_region => $value) {
    if ($grid_region == 'content') {
      if (strpos($variables['region'], 'content') === 0) {
        $variables['classes_array'][] = '';
      }
    }
     else {
      if (strpos($variables['region'], $grid_region) === 0) {
        $variables['classes_array'][] = implode(' ', $grid['regions'][$grid_region]['width']);
      }
    }
  }

  // Sidebar regions get some extra classes and a common template suggestion.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['classes_array'][] = 'column';
    $variables['classes_array'][] = 'sidebar';
    $variables['classes_array'][] = 'sidebar-inner';
    // Allow a region-specific template to override aether's region--sidebar.
    array_unshift($variables['theme_hook_suggestions'], 'region__sidebar');
  }

  // Branding region gets a common template suggestion.
  if (strpos($variables['region'], 'branding') === 0) {
    $variables['classes_array'][] = 'branding-inner';
    // Allow a region-specific template to override aether's region--branding.
    array_unshift($variables['theme_hook_suggestions'], 'region__branding');
  }

  // main_menu region gets a common template suggestion.
  if (strpos($variables['region'], 'main_menu') === 0) {
    $variables['classes_array'][] = 'main-menu-inner';
    // Allow a region-specific template to override aether's region--main_menu.
    array_unshift($variables['theme_hook_suggestions'], 'region__main_menu');
  }

  // Header regions get a common template suggestion.
  if (strpos($variables['region'], 'header') === 0) {
    $variables['classes_array'][] = 'header-inner';
    // Allow a region-specific template to override aether's region--header.
    array_unshift($variables['theme_hook_suggestions'], 'region__header');
  }

  // navigation regions gets a common template suggestion.
  if (strpos($variables['region'], 'navigation') === 0) {
    $variables['classes_array'][] = 'navigation-inner';
    // Allow a region-specific template to override aether's region--sidebar.
    array_unshift($variables['theme_hook_suggestions'], 'region__navigation');
  }

  // Footer regions gets a common template suggestion.
  if (strpos($variables['region'], 'footer') === 0) {
    $variables['classes_array'][] = 'footer-inner';
    // Allow a region-specific template to override aether's region--sidebar.
    array_unshift($variables['theme_hook_suggestions'], 'region__footer');
  }

  // Set region variables.
  // $variables['region_style'] = $variables['fluid_width'] = '';
  $variables['region_name'] = str_replace('_', '-', $variables['region']);
  $variables['classes_array'][] = $variables['region_name'];

}
