<?php
/**
 * @file
 * Implements utility functions.
 */

/**
 * Assigns variables to a static variable to pass them around.
 *
 * @return
 *   A variable from another function.
 */
function _aether_var($var_name, $new_val = NULL) {
  $variables = &drupal_static(__FUNCTION__, array());

  // If a new value has been passed.
  if ($new_val) {
    $variables[$var_name] = $new_val;
  }

  return isset($variables[$var_name]) ? $variables[$var_name] : NULL;
}

/**
 * Returns an empty array if block is empty.
 */
function _aether_region_list($region) {
  $region_list = array();

  // Create an empty array if there are no entries.
  if (empty($region)) {
    $region_list = array();
  }
  else {
    $region_list = 1;
  }

  return $region_list;
}

/**
 * Interlace two or more arrays together.
 */
function _aether_array_interlace() {
  $args = func_get_args();
  $total = count($args);

  if ($total < 2) {
    return FALSE;
  }

  $i = 0;
  $j = 0;
  $arr = array();

  foreach ($args as $arg) {
    foreach ($arg as $v) {
      $arr[$j] = $v;
      $j += $total;
    }

    $i++;
    $j = $i;
  }

  ksort($arr);
  return array_values($arr);
}
