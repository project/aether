<?php
/**
 * @file
 * Implements theme functions.
 */

/**
 * Caches theme real theme or a delta template.
 *
 * @param $theme
 *   The key (machin-readable name) of a theme.
 *
 * @return
 *   The return value of cache_set().
 *
 * @see
 *   cache_set().
 */
function aether_cache_set($theme) {
  $cache = new stdClass();
  
  foreach (array_keys($theme->cacheable()) as $item) {
    if (isset($theme->$item)) {
      $cache->$item = $theme->$item;
    }
  }
  
  if (isset($theme->delta)) {
    return cache_set('aether:' . $theme->theme . ':' . $theme->delta, $cache);
  }
  else {
    return cache_set('aether:' . $theme->theme, $cache);
  }
}


/**
 * A helper function for retrieving region settings.
 * 
 * @param $name
 *    The name of the setting that you want to retrieve. 
 * @param $region
 *    The region that you want to fetch the setting for. 
 * @param $default (optional)
 *   The name (key) of the theme that you want to fetch the
 *   setting for. Defaults to NULL.   
 * @param $theme (optional)
 *   The key (machin-readable name) of a theme. Defaults to the key of the
 *   current theme if not defined.
 *   
 * @see 
 *   alpha_theme_get_setting().
 * 
 * @see
 *   theme_get_setting().
 */
function aether_grid_get_setting($name, $region, $size, $default = NULL, $theme = NULL) {
  $setting = theme_get_setting($region . '_' . $name . '_' . $size, $theme);
  
  return isset($setting) ? $setting : $default;
}

/**
 * A helper function that returns an array of un-supported Drupal core regions.
 */
function aether_regions_exclude() {
  return array('page_top', 'page_bottom');
}

/**
 * A helper function for retrieving the content of theme .info files
 * in the theme trail of $key.
 * 
 * @param $item
 *   The name of the variable that you want to fetch.
 * @param $theme
 *   The key (machin-readable name) of a theme.
 * @return
 *   The $item content of all themes .info files in the theme trail.
 */
function aether_info($item, $theme) {
  $themes = list_themes();
  
  if (!empty($themes[$theme]->info[$item])) {
    return $themes[$theme]->info[$item];
  }
}

/**
 * A wrapper function for theme_get_settings().
 * 
 * @param $name
 *   The name of the setting that you want to retrieve. 
 * @param $default (optional)
 *   The name (key) of the theme that you want to fetch the
 *   setting for. Defaults to NULL.   
 * @param $theme (optional)
 *   The key (machin-readable name) of a theme. Defaults to the key of the
 *   current theme if not defined.
 *   
 * @see 
 *   theme_get_setting().
 */
function aether_theme_get_setting($name, $default = NULL, $theme = NULL) {
  $setting = theme_get_setting($name, $theme);
  
  return isset($setting) ? $setting : $default; 
}

/**
 * This function "fixes" drupal_alter so it also works in the theme-settings and anywhere else 
 * where you want to be 100% certain that drupal_alter uses the proper global $theme.
 * 
 * The problem with drupal_alter is, that it always relies on the global $theme while
 * the theme-settings page relies (and "overrides") the global $theme_key variable while
 * building its form.
 * 
 * @param $type
 * @param $data
 * @param $context1
 * @param $context2
 * 
 * @see
 *   See drupal_alter() for more information about how this works.
 */
function aether_alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
  global $theme, $base_theme_info;

  if ($theme != $context1) {
    $themes = list_themes();
    
    if (!empty($themes[$context1])) {
      $theme_original = $theme;
      $base_theme_info_original = $base_theme_info;
      
      foreach (aether_theme_trail($context1) as $key => $title) {
        if (isset($themes[$key])) {
          $base_theme_info[$key] = $themes[$key];
        }
      }
      
      $functions = &drupal_static('drupal_alter');
      
      if (!empty($base_theme_info)) {
        foreach ($base_theme_info as $item) {
          if (is_file(drupal_get_path('theme', $item->name) . '/template.php')) {
            include_once drupal_get_path('theme', $item->name) . '/template.php';
          }
        }
      }
      
      array_pop($base_theme_info);
      
      $theme = $context1;
            
      drupal_alter($type, $data, $context1, $context2);      
      
      $theme = $theme_original;
      $base_theme_info = $base_theme_info_original;
      
      unset($functions[$type]);
    }
  }
  else {
    drupal_alter($type, $data, $context1, $context2);
  }
}

/**
 * Retrieves the cached parts of a theme container.
 *
 * @param $theme
 *   The key (machin-readable name) of a theme.
 * @param $delta (optional)
 *   The machine-readable name of a delta template.
 *
 * @return
 *   A cache object or nothing if no cache entry exists.
 */
function aether_cache_get($theme, $delta = NULL) {
  if (isset($delta)) {
    return cache_get('aether:' . $theme . ':' . $delta);
  }
  else {
    return cache_get('aether:' . $theme);
  }
}

/**
 * Returns the theme container object for the current theme.
 *
 * @return
 *   An object representing the current theme.
 */
function aether_get_theme() {
  $container = &drupal_static(__FUNCTION__);

  $key = $theme = $GLOBALS['theme_key'];
  $delta = NULL;

  if (module_exists('delta') && $delta = delta_get_current($theme)) {
    $key .= ':' . $delta;
  }

  if (!isset($container[$key])) {
    foreach (array_keys(aether_theme_trail($theme)) as $item) {
      if (class_exists($item . '_theme_container')) {
        $class = $item . '_theme_container';
      }
    }

    if (isset($class)) {
      $container[$key] = new $class($theme, $delta);
    }
  }

  return $container[$key];
}

/**
 * Builds the full theme trail (deepest base theme first, subtheme last).
 *
 * @param $theme
 *   The key (machin-readable name) of a theme.
 *
 * @return
 *   An array of all themes in the trail, keyed by theme key.
 */
function aether_theme_trail($theme) {
  $static = &drupal_static(__FUNCTION__);

  if (!isset($static)) {
    $themes = list_themes();

    if (isset($themes[$theme]->info['base theme'])) {
      foreach (system_find_base_themes($themes, $theme) as $base => $name) {
        if ($name && isset($themes[$base])) {
          $static[$theme][$base] = $themes[$base]->info['name'];
        }
      }
    }

    // Add our current subtheme ($key) to that array.
    if (isset($themes[$theme])) {
      $static[$theme][$theme] = $themes[$theme]->info['name'];
    }
  }

  if (isset($static[$theme])) {
    return $static[$theme];
  }
}

/**
 * Implements hook_theme().
 */
function aether_theme() {
  return array(
    'grid_block' => array(
      'variables' => array('content' => NULL, 'id' => NULL),
    ),
  );
}

/**
 *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function aether_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 *
 * @return
 *   A string containing the breadcrumb output.
 */
function aether_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $output = '';

  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('aether_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('aether_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('aether_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('aether_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $breadcrumb[] = check_plain($item['title']);
        }
        else {
          $breadcrumb[] = drupal_get_title();
        }
      }
      elseif (theme_get_setting('aether_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users.
      if (empty($variables['title'])) {
        $variables['title'] = t('You are here');
      }
      // Unless overridden by a preprocess function, make the heading invisible.
      if (!isset($variables['title_attributes_array']['class'])) {
        $variables['title_attributes_array']['class'][] = 'element-invisible';
      }

      // Build the breadcrumb trail.
      $output = '<nav class="breadcrumb" role="navigation">';
      $output .= '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';
      $output .= '<ul><li>' . implode($breadcrumb_separator . '</li><li>', $breadcrumb) . $trailing_separator . '</li></ul>';
      $output .= '</nav>';
    }
  }

  return $output;
}


/**
 * Add unique class (mlid) to all menu items.
 */
function aether_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $element['#attributes']['class'][] = 'menu-' . $element['#original_link']['mlid'];

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Make Drupal core generated images responsive i.e. flexible in width.
 */
function aether_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);

  // Remove width and height attributes.
  foreach (array('alt', 'title') as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  return '<img' . drupal_attributes($attributes) . ' />';
}

// /**
//  * Returns a list of blocks.
//  * Uses Drupal block interface,
//  * Appends any blocks assigned by the Context module.
//  */
// function aether_block_list($region) {
//   $drupal_list = array();

//   if (module_exists('block')) {
//     $drupal_list = block_list($region);
//   }
//   if (module_exists('context') && $context = context_get_plugin('reaction', 'block')) {
//     $context_list = $context->block_list($region);
//     $drupal_list = array_merge($context_list, $drupal_list);
//   }

//   return $drupal_list;
// }

function aether_file($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'file';
  // Change size of file field
  $element['#attributes']['size'] = 27;
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-file'));
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

function aether_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // @aether add a form-label class to all labels
  $label_class = 'label-' . $element['#id'];
  $attributes['class'] = isset($attributes['class']) ? $attributes['class'] . ' ' . $label_class . ' ' . 'form-label' : $label_class .  ' ' . 'form-label';

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

function aether_textfield($variables) {
  // Set the new size of input fields
  $new_size = 40;
  if (isset($variables['element']['#size']) && $variables['element']['#size'] > $new_size) {
    $variables['element']['#size'] = $new_size;
  }
  return theme_textfield(array('element' => $variables['element']));
}

function aether_password($variables) {
  // Set the new size of input fields
  $new_size = 40;
  if (isset($variables['element']['#size']) && $variables['element']['#size'] > $new_size) {
    $variables['element']['#size'] = $new_size;
  }
  return theme_password(array('element' => $variables['element']));
}