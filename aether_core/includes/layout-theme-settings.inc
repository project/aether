<?php
drupal_add_js('jQuery(function () {jQuery("#edit-layout").fieldset_tabs();});', array('type' => 'inline', 'scope' => 'header', 'weight' => 9));
drupal_add_js($aether_theme_path . "js/layout-theme-settings.js", array('type' => 'file', 'scope' => 'header', 'weight' => 10));

$form['aether_settings']['layout'] = array(
  '#title' => t('Layout'),
  '#type' => 'fieldset',
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  '#weight' => 9,
);

if (theme_get_setting('responsive_toggle')) {
  $form['aether_settings']['layout']['#attributes'] = array('class' => array('responsive-on'));
} else {
  $form['aether_settings']['layout']['#attributes'] = array('class' => array('responsive-off'));
}

$form['aether_settings']['layout']['responsive_toggle'] = array(
  '#type'          => 'checkbox',
  '#attributes' => array('class' => array('responsove-toggle')),
  '#title'         => t('Enable or disable responsive features.'),
  '#default_value' => theme_get_setting('responsive_toggle'),
  '#description'   => t("Enable or disable device media queries that aid in making your design !responsive. If you wish to use a fixed width desktop layout, uncheck this option. WARNING: if you disable media queries, you will need to also disable the responsive meta and polyfills.", array('!responsive' => l(t('responsive'), 'http://www.alistapart.com/articles/responsive-web-design/'))),
);

if (theme_get_setting('responsive_toggle')) {
  $media = array();
  $media_queries = theme_get_setting('media_queries');
  if ($media_queries && is_numeric($media_queries)) {
    for ($i = 1; $i <= $media_queries; $i++) {
      $media[] = 'Media' . $i;
    }
  }
}
else {
  $media = array(t('Default'));
  $media_queries = 1;
}

// Calculate grid options.
$gutter_width = (int) drupal_substr(theme_get_setting("gutter_width"), 0, 2);
$grid_width = (int) drupal_substr(theme_get_setting("theme_grid" . '1'), 4, 2);
$grid_type = drupal_substr(theme_get_setting("theme_grid" . '1'), 7);
$grid_width_options = array();
$grid_width_options_pos = array();
for ($i = 1; $i <= floor($grid_width); $i++) {
  $grid_units = $i . (($i == 1) ? ' ' . t('col') . ' ' : ' ' . t('cols') . ' ');
  $grid_width_options[$i] = $grid_units . ((($i * (((int) $grid_type - $gutter_width) / $grid_width)) - $gutter_width) . 'px');
  $grid_width_options_pos[$i] = $grid_units;
}

for ($media_count = 0; $media_count <= ($media_queries - 1); $media_count++) {

  if (theme_get_setting('responsive_toggle')) {
    $size = $grid_settings['sizes'][$media_count];
  } else {
    $size_count = count($grid_settings['prefixes']) - 1;
    $size = $grid_settings['sizes'][$size_count];
  }

  $sidebar_layouts = $grid_layouts[$size]['sidebar_layouts'];

  $form['aether_settings']['layout'][$size] = array(
    '#title' => t('@media', array('@media' => strtoupper($size))),
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('device-layouts')),
  );

  $form['aether_settings']['layout'][$size]['description'] = array(
    '#type' => 'container',
    '#prefix' => '<h4>' . t('Selected grid size: @media', array('@media' => strtoupper($size))) . '</h4>',
    '#attributes' => array('class' => array('row-container')),
    );

  // Sidebar layout.
  $form['aether_settings']['layout'][$size]['grid_sidebars_' . $size] = array(
    '#type'          => 'radios',
    '#attributes'    => array('class' => array('sidebar-layout')),
    '#title'         => t('Choose a sidebar layout:'),
    '#default_value' => $sidebar_layouts,
    '#suffix'        => '<label>' . t('Set column widths:') . '</label>',
    '#options'       => array(
      'splits' => t('Split sidebars'),
      'sidebars_first' => t('Both sidebars first'),
      'sidebars_last' => t('Both sidebars last'),
      's1left_s2bottom' => t('Sidebar1 left, sidebar2 bottom'),
      's1right_s2bottom' => t('Sidebar1 right, sidebar2 bottom'),
      's1left_s2bottom_full' => t('Sidebar1 left, sidebar2 under content'),
      's1right_s2bottom_full' => t('Sidebar1 right, sidebar2 under content'),
      'stacked_right' => t('Sidebar1 and sidebar2 stacked right'),
      'full' => t('Full width'),
    ),
  );

  foreach ($grid_regions as $key => $grid_region) {

    if ((!isset($grid_region)) || !($grid_region == 'content')) {
      list($row_name,$region_name) = explode('_', $grid_region, 2);

      $form['aether_settings']['layout'][$size][$row_name] = array(
        '#title' => t(drupal_ucfirst($row_name) .' Row'),
        '#type' => 'fieldset',
        '#description' => t('Selected grid size: @media' . '<br />' . 'Set the number of columns each region in this row should span.' . '<br />' . 'Note: Regions set to the max grid width are cleared by default.', array('@media' => strtoupper($size))),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#attributes' => array('class' => array('row-layouts')),
      );
    }
  }

  foreach ($grid_regions as $key => $grid_region) {

    if ((!isset($grid_region)) || !($grid_region == 'content')) {
      list($row_name,$region_name) = explode('_', $grid_region, 2);

      $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count] = array(
        '#type' => 'container',
        '#prefix' => '<h4>' . check_plain(drupal_ucfirst($row_name) . ' ' . drupal_ucfirst($region_name)) . '</h4>',
        '#attributes' => array('class' => array('row-container')),
        );

      if (!($grid_region == 'sidebar_first' || $grid_region == 'sidebar_second')) {

        // Region clearing.
        $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count][$grid_region . '_clear_' . $size] = array(
          '#type'          => 'checkbox',
          '#attributes' => array('class' => array('region-clear')),
          '#title'         => t('Clear to next row'),
          '#default_value' => ($regions[$grid_region][$size]['clear']) ? $regions[$grid_region][$size]['clear'] : 0,
        );

        // Region position.
        $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count][$grid_region . '_position_' . $size] = array(
          '#type'          => 'select',
          '#title'         => t('Position'),
          '#attributes'    => array('class' => array('col-position')),
          '#default_value' => $regions[$grid_region][$size]['position'],
          '#options'       => $grid_width_options_pos,
        );
        $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count][$grid_region . '_position_' . $size]['#options'][$defaults[$grid_region . '_position_' . $size]] .= ' ' . t('- Default');

      }

        // Region width.
        $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count][$grid_region . '_width_' . $size] = array(
          '#type'          => 'select',
          '#title'         => t('Width'),
          '#attributes'    => array('class' => array('col-width')),
          '#default_value' => $regions[$grid_region][$size]['width'],
          '#options'       => $grid_width_options,
        );
        $form['aether_settings']['layout'][$size][$row_name][$grid_region . $media_count][$grid_region . '_width_' . $size]['#options'][$defaults[$grid_region . '_width_' . $size]] .= ' ' . t('- Default');

      }


  }
}