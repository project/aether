<?php
/**
 * @file
 * Grid data created and processed.
 */

/**
 * Generate initial grid info.
 */
function aether_grid_info() {

  global $theme_key;
  $theme = aether_get_theme();

  // Get grid regions from theme settings.
  $grid_regions = $theme->grid_regions;
  $grid_layouts = $theme->grid_layouts;
  $grid_settings = $theme->grid_settings;

  $grid = array();
  $grid['regions'] = array();
  $pos_prefix = 'p';
  $first_prefix = 'f';
  $last_prefix = 'l';
  $right_prefix = 'right';
  $col_prefix = 'c';

  if (theme_get_setting('responsive_toggle')) {
    $media_queries = theme_get_setting('media_queries');
  }
  else {
    $media_queries = 1;
  }

  for ($media_count = 0; $media_count <= ($media_queries - 1); $media_count++) {
    if (theme_get_setting('responsive_toggle')) {
      $size = $grid_settings['sizes'][$media_count];
      $prefix = $grid_settings['prefixes'][$media_count];
    } else {
      $prefix_count = count($grid_settings['prefixes']) - 1;
      $size_count = count($grid_settings['prefixes']) - 1;
      $prefix = $grid_settings['prefixes'][$prefix_count];
      $size = $grid_settings['sizes'][$size_count];
    }
    $columns = $grid_settings['columns'][$media_count];
    $sidebar_layout = $grid_layouts[$size]['sidebar_layouts'];

    // Function block_list does not return the correct enabled regions on demo page.
    $item = menu_get_item();
    if ($item['path'] == 'admin/structure/block/demo/' . $theme_key) {
      $grid['sidebar_first'][$size]['width'] = $grid_regions['sidebar_first'][$size]['width'];
      $grid['sidebar_second'][$size]['width'] = $grid_regions['sidebar_second'][$size]['width'];
    } else {
      $grid['sidebar_first'][$size]['width'] = (_aether_var('sidebar_first')) ? $grid_regions['sidebar_first'][$size]['width'] : 0;
      $grid['sidebar_second'][$size]['width'] = (_aether_var('sidebar_second')) ? $grid_regions['sidebar_first'][$size]['width'] : 0;
    }
    $grid['content'][$size]['width'] = $columns - ($grid['sidebar_first'][$size]['width'] + $grid['sidebar_second'][$size]['width']);

    // Split sidebars.
    if ($sidebar_layout == 'splits') {
      $content_pos[] = $prefix . $pos_prefix . ($grid['sidebar_first'][$size]['width'] + 1);
      $content_width[] = $prefix . $col_prefix . $grid['content'][$size]['width'];
      $sidebar_first_pos[] = $prefix . $pos_prefix . '1';
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_pos[] = $prefix . $pos_prefix . (($grid['sidebar_first'][$size]['width'] + $grid['content'][$size]['width']) + 1);
      $sidebar_second_width[] = $prefix . $col_prefix . $grid['sidebar_second'][$size]['width'];
    }
    // Two sidebars left.
    elseif ($sidebar_layout == 'sidebars_first') {
      $content_pos[] = $prefix . $pos_prefix . (($grid['sidebar_first'][$size]['width'] + $grid['sidebar_second'][$size]['width']) + 1);
      $content_width[] = $prefix . $col_prefix . $grid['content'][$size]['width'];
      $sidebar_first_pos[] = $prefix . $pos_prefix . '1';
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_pos[] = $prefix . $pos_prefix . ($grid['sidebar_first'][$size]['width'] + 1);
      $sidebar_second_width[] = $prefix . $col_prefix . $grid['sidebar_second'][$size]['width'];
    }
    // Two sidebars right.
    elseif ($sidebar_layout == 'sidebars_last') {
      $content_pos[] = $prefix . $pos_prefix . '1';
      $content_width[] = $prefix . $col_prefix . $grid['content'][$size]['width'];
      $sidebar_first_pos[] = $prefix . $pos_prefix . ($grid['content'][$size]['width'] + 1);
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_pos[] = $prefix . $pos_prefix . (($grid['content'][$size]['width'] + $grid['sidebar_first'][$size]['width']) + 1);
      $sidebar_second_width[] = $prefix . $col_prefix . $grid['sidebar_second'][$size]['width'];
    }
    // Sidebar1 left, sidebar2 bottom.
    elseif ($sidebar_layout == 's1left_s2bottom') {
      $content_pos[] = $prefix . $pos_prefix . ($grid['sidebar_first'][$size]['width'] + 1);
      $content_width[] = $prefix . $col_prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $sidebar_first_pos[] = $prefix . $pos_prefix . '1';
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_width[] = $prefix . $col_prefix . $columns;
    }
    // Sidebar1 right, sidebar2 bottom.
    elseif ($sidebar_layout == 's1right_s2bottom') {
      $content_width[] = $prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $sidebar_first_width[] = $prefix .$col_prefix .  $grid['sidebar_first'][$size]['width'];
      $sidebar_first_width[] = $prefix . $last_prefix;
      $sidebar_second_width[] = $prefix . $col_prefix . $columns;
    }
    // Sidebar1 left, sidebar2 under content.
    elseif ($sidebar_layout == 's1left_s2bottom_full') {
      $content_width[] = $prefix . $col_prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $content_width[] = $prefix . $right_prefix;
      $content_width[] = $prefix . $last_prefix;
      $sidebar_first_pos[] = $prefix . $pos_prefix . '1';
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_width[] = $prefix . $col_prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $sidebar_second_width[] = $prefix . $right_prefix;
      $sidebar_second_width[] = $prefix . $last_prefix;
    }
    // Sidebar1 right, sidebar2 under content.
    elseif ($sidebar_layout == 's1right_s2bottom_full') {
      $content_pos[] = $prefix . $pos_prefix . '1';
      $content_width[] = $prefix . $col_prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_first_width[] = $prefix . $last_prefix;
      $sidebar_first_width[] = $prefix . $right_prefix;
      $sidebar_second_width[] = $prefix . $col_prefix . ($grid['content'][$size]['width'] + $grid['sidebar_second'][$size]['width']);
      $sidebar_second_pos[] = $prefix . $pos_prefix . '1';
      $sidebar_second_width[] = $prefix . $first_prefix;
    }
    // Sidebar1 and sidebar2 right stacked.
    elseif ($sidebar_layout == 'stacked_right') {
      if (aether_block_list('sidebar_first') || aether_block_list('sidebar_second')) {
        $content_width[] = $prefix . $col_prefix . ($columns - $grid['sidebar_first'][$size]['width']);
      } else {
        $content_width[] = $prefix . $col_prefix . $columns;
      }
      $sidebar_first_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_first_width[] = $prefix . $last_prefix;
      $sidebar_second_width[] = $prefix . $col_prefix . $grid['sidebar_first'][$size]['width'];
      $sidebar_second_width[] = $prefix . $right_prefix;
      $sidebar_second_width[] = $prefix . $last_prefix;
    }
    // Full width.
    else {
      $content_width[] = $prefix . $col_prefix . $columns;
      $sidebar_first_width[] = $prefix . $col_prefix . $columns;
      $sidebar_second_width[] = $prefix . $col_prefix . $columns;
    }

    foreach ($grid_regions as $grid_region => $value) {
      if ($grid_region != 'content' || $grid_region != 'sidebar_first' || $grid_region != 'sidebar_second') {
        ${$grid_region . '_pos'}[] = $prefix . $pos_prefix . $grid_regions[$grid_region][$size]['position'];
        ${$grid_region . '_width'}[] = $prefix . $col_prefix . $grid_regions[$grid_region][$size]['width'];
        if ($grid_regions[$grid_region][$size]['clear']) {
          ${$grid_region . '_clear'}[] = $prefix . $first_prefix;
        } else {
          ${$grid_region . '_clear'}[] = '';
        }
      }

    }

  }

  foreach ($grid_regions as $grid_region => $value) {

    $region_width = _aether_array_interlace(${$grid_region . '_width'}, ${$grid_region . '_pos'}, ${$grid_region . '_clear'});

    $grid['regions'][$grid_region] = array(
      'width' => array_filter($region_width),
    );
  }

  return $grid;

}
