<?php

/**
 * @file
 * Container class for theme configuration.
 */

class aether_theme_container {
  var $theme;
  // var $settings;
  var $grid_settings;
  var $grid_layouts;
  var $grid_regions;
  
  /**
   * @todo
   */
  function __construct($theme, $delta = NULL) {
    $this->theme = $theme;
    $this->delta = $delta;
    

    if ($cache = aether_cache_get($theme, $delta)) {
      foreach ($cache->data as $key => $item) {
        $this->$key = $item;
      }
    }

    foreach ($this->cacheable() as $item => $required) {
      if ($required && !isset($this->$item)) {
        $this->init();
        
        aether_alter('aether_pre_cache', $this, $theme, $delta);
        aether_cache_set($this);
        
        return;
      }
    }
    
    aether_alter('aether', $this, $theme, $delta);
  }
  
  /**
   * @todo
   */
  function init() {
    // $this->settings();
    $this->grid_regions();
    $this->grid_settings();
    $this->grid_layouts();
  }

  /**
   * @todo
   */
  function grid_regions() {
    if (!isset($this->grid_regions)) {
      $this->grid_regions = array();
      $regions = theme_get_setting('grid_region', $this->theme);
      $sizes = theme_get_setting('grid_size', $this->theme);
      foreach ($regions as $region) {
        foreach ($sizes as $size) {
          $this->grid_regions[$region][$size] = array(
            'region' => $region,
            'position' => aether_grid_get_setting('position', $region, $size),
            'width' => aether_grid_get_setting('width', $region, $size),
            'clear' => aether_grid_get_setting('clear', $region, $size),
          );
        }
      }

      aether_alter('aether_regions', $this->grid_regions, $this->theme);
    }

    return $this->grid_regions;
  }

  /**
   * @todo
   */
  function grid_layouts() {
    if (!isset($this->grid_layouts)) {
    $this->grid_layouts = array();
    $sizes = theme_get_setting('grid_size', $this->theme);
    
    foreach ($sizes as $size) {
      $this->grid_layouts[$size] = array(
        'sidebar_layouts' => aether_theme_get_setting('grid_sidebars_' . $size),
      );
    }

      aether_alter('aether_grid_layouts', $this->grid_layouts, $this->theme);
    }
    
    return $this->grid_layouts;
  }

  /**
   * @todo
   */
  function grid_settings() {
    if (!isset($this->grid_settings)) {

    $this->grid_settings = array(
      'prefixes' => aether_theme_get_setting('grid_prefix', array(), $this->theme),
      'sizes' => aether_theme_get_setting('grid_size', array(), $this->theme),
      'regions' => aether_theme_get_setting('grid_region', array(), $this->theme),
      'columns' => aether_theme_get_setting('grid_columns', array(), $this->theme),
    );

      aether_alter('aether_grid_settings', $this->grid_settings, $this->theme);
    }
    
    return $this->grid_settings;
  }

  // /**
  //  * @todo
  //  */
  // function settings() {
  //   if (!isset($this->settings)) {

  //     // $this->settings = array(
  //     //   'grid' => aether_theme_get_setting('grid', FALSE, $this->theme),
  //     //   // 'css' => aether_theme_get_setting('aether_css', array(), $this->theme),
  //     //   // 'libraries' => aether_theme_get_setting('aether_libraries', array(), $this->theme),     
  //     //   // 'responsive' => aether_theme_get_setting('aether_responsive', FALSE, $this->theme),
  //     //   // 'hidden' => array(),
  //     //   // 'viewport' => array(
  //     //   //   'enabled' => aether_theme_get_setting('aether_viewport', FALSE, $this->theme),
  //     //   //   'initial' => aether_theme_get_setting('aether_viewport_initial_scale', 1, $this->theme),
  //     //   //   'min' => aether_theme_get_setting('aether_viewport_min_scale', 1, $this->theme),
  //     //   //   'max' => aether_theme_get_setting('aether_viewport_max_scale', 1, $this->theme),
  //     //   //   'user' => aether_theme_get_setting('aether_viewport_user_scaleable', TRUE, $this->theme),
  //     //   // ),
  //     //   // 'debug' => array(
  //     //   //   'block' => aether_theme_get_setting('aether_debug_block_toggle', FALSE, $this->theme),
  //     //   //   'block_active' => aether_theme_get_setting('aether_debug_block_active', FALSE, $this->theme),
  //     //   //   'grid' => aether_theme_get_setting('aether_debug_grid_toggle', FALSE, $this->theme),
  //     //   //   'grid_active' => aether_theme_get_setting('aether_debug_grid_active', FALSE, $this->theme),
  //     //   //   'roles' => array_keys(array_filter(aether_theme_get_setting('aether_debug_grid_roles', array(), $this->theme))),
  //     //   // ),
  //     // );

  //     aether_alter('aether_settings', $this->settings, $this->theme);
  //   }
    
  //   return $this->settings;
  // }
  
  /**
   * @todo
   */
  function cacheable() {
    $cacheable = array_fill_keys(array('settings'), TRUE);
    
    aether_alter('aether_cacheable', $cacheable, $this->theme);
    
    return $cacheable;
  }
}