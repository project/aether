<?php
/**
 * @file
 * General alters.
 */

/*
 * Login block modifications
 * Allows for link text modification, form weights and placeholder text.
*/
function aether_form_user_login_block_alter(&$form, &$form_state){
  $item = array();
  if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
    $items[] = l(t('Create new account'), 'user/register', array('attributes' => array('title' => t('Create a new user account.'))));
  }
  $items[] = l(t('Forgot password?'), 'user/password', array('attributes' => array('title' => t('If you forgot your password, click to request a new password via e-mail.'))));
  $form['links']['#markup'] = theme('item_list', array('items' => $items));
  $form['links']['#weight'] = 10000;

  // HTML5 placeholder attributes
  $form['name']['#attributes']['placeholder'] = $form['name']['#title'];
  $form['pass']['#attributes']['placeholder'] = $form['pass']['#title'];
}

function aether_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    // HTML5 placeholder attribute
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  }
}

/**
 * Implements hook_html_head_alter().
 */
function aether_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  $head['system_meta_content_type']['#attributes'] = array(
    'charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']),
  );
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Prevent user-facing field styling from screwing up node edit forms by
 * renaming the classes on the node edit form's field wrappers.
 */
function aether_form_node_form_alter(&$form, &$form_state, $form_id) {
  // Remove if #1245218 is backported to D7 core.
  foreach (array_keys($form) as $item) {
    if (strpos($item, 'field_') === 0) {
      if (!empty($form[$item]['#attributes']['class'])) {
        foreach ($form[$item]['#attributes']['class'] as &$class) {
          if (strpos($class, 'field-type-') === 0 || strpos($class, 'field-name-') === 0) {
            // Make the class different from that used in theme_field().
            $class = 'form-' . $class;
          }
        }
      }
    }
  }
}


/**
 * Implements hook_page_alter().
 *
 * Force enable empty regions where necessary if they are in the page array.
 *
 * Look for the last block in the region. This is impossible to determine from
 * within a preprocess_block function.
 *
 * @param $page
 *   Nested array of renderable elements that make up the page.
 */
function aether_page_alter(&$page) {

  // Check if the branding region is in the page array and display it if so.
  foreach (system_region_list($GLOBALS['theme'], REGIONS_ALL) as $region => $name) {
    if (in_array($region, array('branding'))) {
      $page['branding'] = array(
        '#region' => 'branding',
        '#weight' => '-10',
        '#theme_wrappers' => array('region'),
      );
    }
    if (in_array($region, array('main_menu'))) {
      $page['main_menu'] = array(
        '#region' => 'main_menu',
        '#weight' => '-10',
        '#theme_wrappers' => array('region'),
      );
    }
  }

  // Look in each visible region for blocks.
  foreach (system_region_list($GLOBALS['theme'], REGIONS_VISIBLE) as $region => $name) {
    if (!empty($page[$region])) {
      // Find the last block in the region.
      $blocks = array_reverse(element_children($page[$region]));
      while ($blocks && !isset($page[$region][$blocks[0]]['#block'])) {
        array_shift($blocks);
      }
      if ($blocks) {
        $page[$region][$blocks[0]]['#block']->last_in_region = TRUE;
      }
    }
  }
}

/**
 * Implements hook_css_alter().
 */
function aether_css_alter(&$css) {
  global $language;
  global $theme_key;

  if (theme_get_setting('exclude_stylesheets')) {
    if ($exclude = array_flip(theme_get_setting('exclude_stylesheets'))) {

      if ($language->direction == LANGUAGE_LTR) {
        foreach ($exclude as $basename) {
          $rtl = str_replace('.css', '-rtl.css', $basename);
          $exclude[$rtl] = $rtl;
        }
      }

      foreach($css as $key => $item) {
        if (isset($exclude[$key])) {
          unset($css[$key]);
        }
      }

    }
  }
}
